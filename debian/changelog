php-uopz (7.1.1++-4) unstable; urgency=medium

  * Upload to unstable

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 19:47:01 +0200

php-uopz (7.1.1++-4~exp4) experimental; urgency=medium

  * Rebuild with PHP 8.4.0RC1
  * Pull upstream patches to fix FTBFS and tests

 -- Ondřej Surý <ondrej@debian.org>  Fri, 27 Sep 2024 16:30:42 +0200

php-uopz (7.1.1++-4~exp3) experimental; urgency=medium

  * Fix Build-Depend on php-all-dev (>= 2:95~)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 24 Sep 2024 14:49:45 +0200

php-uopz (7.1.1++-4~exp2) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:40 +0200

php-uopz (7.1.1++-4~exp1) experimental; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Mon, 23 Sep 2024 18:49:37 +0200

php-uopz (7.1.1++-4) unstable; urgency=medium

  * Bump Build-Depends to dh-php >= 5.5~

 -- Ondřej Surý <ondrej@debian.org>  Sun, 22 Sep 2024 08:12:49 +0200

php-uopz (7.1.1++-3) unstable; urgency=medium

  * Upload to experimental

 -- Ondřej Surý <ondrej@debian.org>  Sat, 21 Sep 2024 12:57:33 +0200

php-uopz (7.1.1++-2) unstable; urgency=medium

  * Bump default version to PHP 8.4

 -- Ondřej Surý <ondrej@debian.org>  Sat, 06 Jul 2024 17:34:04 +0200

php-uopz (7.1.1++-1) unstable; urgency=medium

  * New upstream version 7.1.1
  * Finish splitting the source package for PHP 7.x and PHP 8.x

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 18:05:38 +0200

php-uopz (7.1.1+6.1.2-7) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 14:12:04 +0100

php-uopz (7.1.1+6.1.2-6) unstable; urgency=medium

  * Remove the default version override

 -- Ondřej Surý <ondrej@debian.org>  Tue, 18 Jan 2022 23:07:30 +0100

php-uopz (7.1.1+6.1.2-5) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:35 +0100

php-uopz (7.1.1+6.1.2-4) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:10:00 +0100

php-uopz (7.1.1+6.1.2-2) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:26:07 +0100

php-uopz (7.1.1+6.1.2-1) unstable; urgency=medium

  * New upstream version 7.1.1+6.1.2

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:14:44 +0100

php-uopz (7.0.0+6.1.2-2) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:08 +0100

php-uopz (7.0.0+6.1.2-1) unstable; urgency=medium

  * New upstream version 7.0.0+6.1.2

 -- Ondřej Surý <ondrej@debian.org>  Mon, 02 Aug 2021 09:47:30 +0200

php-uopz (6.1.2-10) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:08:11 +0100

php-uopz (6.1.2-9) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:08:14 +0100

php-uopz (6.1.2-8) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:51 +0100

php-uopz (6.1.2-7) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:29 +0100

php-uopz (6.1.2-6) unstable; urgency=medium

  * Remove conflict with xdebug from package.xml

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:16:35 +0100

php-uopz (6.1.2-5) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:11:08 +0100

php-uopz (6.1.2-4) unstable; urgency=medium

  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:41:27 +0100

php-uopz (6.1.2-3) unstable; urgency=medium

  * Update for dh-php >= 2.0 support
  * Build the extension only for PHP 7.1+

 -- Ondřej Surý <ondrej@debian.org>  Sat, 17 Oct 2020 07:10:21 +0200

php-uopz (6.1.2-2) unstable; urgency=medium

  * Don't fail on failed tests

 -- Ondřej Surý <ondrej@debian.org>  Tue, 07 Jul 2020 11:18:36 +0200

php-uopz (6.1.2-1) unstable; urgency=medium

  * New upstream version 6.1.2
  * Finish conversion to debhelper compat level 10

 -- Ondřej Surý <ondrej@debian.org>  Tue, 07 Jul 2020 11:02:18 +0200

php-uopz (6.1.1-1) unstable; urgency=low

  * Initial release.

 -- Ondřej Surý <ondrej@debian.org>  Thu, 19 Dec 2019 11:46:35 +0100
